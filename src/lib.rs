#![allow(unused, non_camel_case_types)]

pub mod m
  { #[macro_export] macro_rules! bE
      { ($c:expr; $t:expr; $e:expr)               => (if $c {$t} else {$e});
        ($c:expr; $t:expr;        )               => (if $c {$t} else {  });
        ($c:expr;        ; $e:expr)               => (if $c {  } else {$e});               }
    #[macro_export] macro_rules! oE
      { ($o:expr; $x:pat,$s:expr; $n:expr)        => (match $o {Some($x)=>$s, None=>$n});
        ($o:expr; $x:pat,$s:expr;        )        => (match $o {Some($x)=>$s, None=>()});
        ($o:expr;               ; $n:expr)        => (match $o {Some(x) =>x,  None=>$n});  }
    #[macro_export] macro_rules! rE
      { ($r:expr; $x:pat,$o:expr; $y:pat,$e:expr) => (match $r {Ok($x)=>$o, Err($y)=>$e});
        ($r:expr; $x:pat,$o:expr;               ) => (match $r {Ok($x)=>$o, Err(_) =>()});
        ($r:expr;               ; $y:pat,$e:expr) => (match $r {Ok(x) =>x,  Err($y)=>$e}); }

    #[macro_export] macro_rules! oR
      { ($x:expr; $r:expr) => (oE![$x;; r![$r]]);
        ($x:expr;        ) => (oE![$x;; r![Default::default()]]);          }
    #[macro_export] macro_rules! rR
      { ($x:expr; $y:pat,$r:expr) => (rE![$x;; $y,r![$r]]);
        ($x:expr;               ) => (rE![$x;; _,r![Default::default()]]); }

    #[macro_export] macro_rules! a {($a:expr) => (assert!($a))}
    #[macro_export] macro_rules! uw {($u:expr) => (($u).unwrap())}
    
    #[macro_export] macro_rules! l
      { ($($x:pat = $v:expr),+) => ($(let $x = $v);+;) }
    #[macro_export] macro_rules! m
      { ($e:expr) => (&mut $e);  ($e:expr, $r:expr) => (&mut $e[$r]) }
    #[macro_export] macro_rules! M
      { ($e:expr; $($p:pat,$v:expr);+) => (match $e {$($p => $v),+}); }
    #[macro_export] macro_rules! r
      { () => (return);  ($e:expr) => (return $e);
        ($c:expr; $e:expr) => (bE![$c; return $e;]);
        ($c:expr;) => (bE![$c; return Default::default();]); }
    #[macro_export] macro_rules! br
      { () => (break);  ($e:expr) => (break $e);
        ($c:expr; $e:expr) => (bE![$c; break $e;]);
        ($c:expr;) => (bE![$c; break;]);            }
    #[macro_export] macro_rules! ct
      { () => (continue);  ($c:expr) => (bE![$c; continue;]) }
    #[macro_export] macro_rules! f
      { (       $v:pat=$g:expr; $b:expr) => (for $v in $g {$b});
        ($i:pat,$v:pat=$g:expr; $b:expr) => (for ($i,$v) in $g.into_iter().enumerate() {$b}); }

    #[macro_export] macro_rules! u8  {($n:expr) => ($n as u8) }
    #[macro_export] macro_rules! i8  {($n:expr) => ($n as i8) }
    #[macro_export] macro_rules! u16 {($n:expr) => ($n as u16)}
    #[macro_export] macro_rules! i16 {($n:expr) => ($n as i16)}
    #[macro_export] macro_rules! u32 {($n:expr) => ($n as u32)}
    #[macro_export] macro_rules! i32 {($n:expr) => ($n as i32)}
    #[macro_export] macro_rules! u64 {($n:expr) => ($n as u64)}
    #[macro_export] macro_rules! i64 {($n:expr) => ($n as i64)}
    #[macro_export] macro_rules! uz  {($n:expr) => ($n as uz) }
    #[macro_export] macro_rules! iz  {($n:expr) => ($n as uz) }

    pub(crate) use
      { bE,oE,rE, oR,rR, a,uw,l,m,M,r,br,ct,f, u8,u16,u32,u64,uz, i8,i16,i32,i64,iz };          }

pub type uz = usize;  pub type iz = isize;
pub type O<T> = Option<T>;  pub type R<T,E> = Result<T,E>;
pub const T:bool = true;  pub const F:bool = false;
